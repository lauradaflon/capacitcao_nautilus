# Guide of use #

**How to use the code:**
The code is designed to a car formed by Arduino Nano V3 Atmega328, DC motors and Hbridge L298. But you can teste the code in a more broad way going to [Tinkercad](https://www.tinkercad.com/). After starting, each number will represent an action:

1. Backward-left
2. Backward
3. Backward-right
4. Turn left
5. Stop
6. Turn right
7. Foward-left
8. Foward
9. Foward-right

**How the code works:**
The code control the motors in two of the three wheels that are in the car. With this it can make the car changes it's direction. 